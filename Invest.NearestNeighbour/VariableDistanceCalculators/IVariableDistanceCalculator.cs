﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invest.NearestNeighbour
{
    /// <summary>
    /// The interface for classes that calculate the distance between two promotions for a sepcific attribute
    /// Acumen to code the specific implementations - likely to be 6 or 7 - logic is still being defined
    /// </summary>
    public interface IVariableDistanceCalculator
    {
        VariableDistance CalculateVariableDistance(INNPromotion mainPromotion, INNPromotion promoBeingTested);
    }
}
