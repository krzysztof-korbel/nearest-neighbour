﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invest.NearestNeighbour
{
    /// <summary>
    /// Represents the "distance" between two promotions for a specific variable
    /// </summary>
    public class VariableDistance
    {
        /// <summary>
        /// Represents the numeric value of the distance
        /// </summary>
        public double Value { get; set; }
    }
}
