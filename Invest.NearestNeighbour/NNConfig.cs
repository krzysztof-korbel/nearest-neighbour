﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invest.NearestNeighbour
{
    public class NNConfig
    {
        public int TopNPromotions { get; set; }
        public Dictionary<NNAttributeEnum, double> NNAttributeWeightsCollection { get; set; }
    }
}
