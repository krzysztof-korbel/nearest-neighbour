﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Invest.NearestNeighbour.PromotionAPI;

namespace Invest.NearestNeighbour
{
    public class Execute
    {
        public void LoadHTMLPopup(int promoBeingTestedId)
        {
            IEnumerable<INNPromotion> promotions;
            NNConfig config;
            IEnumerable<NNPromotion> evaluatedPromotions;

            //get the Nearest Neighbours config
            //TODO: check what parameters we need to pass to get the config
            config = PromotionAPI.PromotionAPI.GetNearestNeighboursConfig();

            // load all promotions via Acumen API
            //TODO: promotions collection should also contain data for the main collection
            promotions = PromotionAPI.PromotionAPI.GetAllPromotions(promoBeingTestedId);

            // calculate distance
            evaluatedPromotions = NNCalculator.Calculate(promoBeingTestedId, promotions, config);            

            // display results
            //TODO: convert to view model
            UpliftPredictionViewModel viewModel = new UpliftPredictionViewModel();             
        }
    }

    public class AttributeViewModel
    {
        string AttributeValue { get; set; }
        double AttributeDistance { get; set; }
        // If you assume the distance of each variable will be a number between 0 (zero distance) and 1 (huge distance) then for now colour 0-.3333 as Green, 0.3334 – 0.6666 as Yellow and 0.6667+ as red.
        string AttributeColour { get; set; }
    }

    public class PromotionViewModel
    {
        double Uplift { get; set; }
        Dictionary<NNAttributeEnum, AttributeViewModel> AttributeVariableDistanceCollection { get; set; }        
        string PromoOverview { get; set; } // will include HTML markup E.g. <STRONG> etc.                      
        int Match { get; set; } 
    }

    public class UpliftPredictionViewModel
    {
        // This is calculated by taking the weighted average of each of the promotions aboive it – weighting it based on the match %, i.e. (1 500 + 0.8 * 450 + .8 450 + .6 * 800) / (1+0.8+0.8+0.6)
        double UpliftPredictionValue { get; set; }
        IEnumerable<PromotionViewModel> PromotionViewModelCollection { get; set; }   
    }
}
