﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invest.NearestNeighbour.PromotionAPI
{
	public class PromotionAPI
	{
		public static int KnownPromoId = 201;
		private static List<INNPromotion> _allPromotions = null;

		public static IEnumerable<INNPromotion> GetAllPromotions(int mainPromotionId)
		{
			if (_allPromotions == null)
			{
				GeneratePromotions();
			}

			return _allPromotions;
		}

		private static void GeneratePromotions()
		{
			var rand = new Random();
			Func<float> distanceGen = () =>
			{
				return rand.Next(0, 100) / 100f;
			};
			var knownPromoAttributes = CreatePromoAttributes();
			var attributesDict = new Dictionary<NNAttributeEnum, INNAttribute>() { { NNAttributeEnum.DiscountDepth, new DiscountDepthAttribute(22) {AttributeName="Discount", AttributeDistance=distanceGen() } },
				{ NNAttributeEnum.DateRange, new DateRangeAttribute(new DateTime(2000,12,1), new DateTime(rand.Next(2000,2016),rand.Next(1,12), rand.Next(1,31)))	{ AttributeName="Date Range", AttributeDistance=distanceGen() } },
				{NNAttributeEnum.Mechanic, new MechanicAttribute(3) { AttributeName = "Mechanic", AttributeDistance=distanceGen()} },
				{NNAttributeEnum.ProductScope, new ProductScopeAttribute() {AttributeName="Products", AttributeDistance=distanceGen() } }
			};
			_allPromotions = new List<INNPromotion>()
			{
				new NNPromotion(KnownPromoId, rand.Next(0,1000),  "Lorem",knownPromoAttributes) {
				},
				new NNPromotion(rand.Next(),rand.Next(0,1000),  "<p>Lorem ipsum...</p>",attributesDict.ToDictionary(attr=>attr.Key, attr=>attr.Value)) {
				},
				new NNPromotion(rand.Next(), rand.Next(0,1000),  "<p>Lorem ipsum...</p>",attributesDict.ToDictionary(attr=>attr.Key, attr=>attr.Value)) {
				},
				new NNPromotion(rand.Next(), rand.Next(0,1000),  "<p>Lorem ipsum...</p>",attributesDict.ToDictionary(attr=>attr.Key, attr=>attr.Value)) {
				},
				new NNPromotion(rand.Next(), rand.Next(0,1000),  "<p class='bold'>AgriCola 20% off</h1>"+
						"<p>Customer: morrisons</p>"+
                        "<p>20 % discount</p>"+
                        "<p>AgriCola</p>",attributesDict.ToDictionary(attr=>attr.Key, attr=>attr.Value)) {
				},
				new NNPromotion(rand.Next(),rand.Next(0,1000),  "<p class='bold'>AgriCola 20% off</h1>"+
						"<p>Customer: morrisons</p>"+
						"<p>20 % discount</p>"+
						"<p>AgriCola</p>",attributesDict.ToDictionary(attr=>attr.Key, attr=>attr.Value)) {
				},
				new NNPromotion(rand.Next(), rand.Next(0,1000),  "<p>Lorem ipsum...</p>",attributesDict.ToDictionary(attr=>attr.Key, attr=>attr.Value)) {
				},
				new NNPromotion(rand.Next(), rand.Next(0,1000),  "<p class='bold'>AgriCola 20% off</h1>"+
						"<p>Customer: morrisons</p>"+
                        "<p>20 % discount</p>"+
                        "<p>AgriCola</p>",attributesDict.ToDictionary(attr=>attr.Key, attr=>attr.Value)) {
				},
				new NNPromotion(rand.Next(), rand.Next(0,1000),  "<p>Lorem ipsum...</p>",attributesDict.ToDictionary(attr=>attr.Key, attr=>attr.Value)) {
				},
				new NNPromotion(rand.Next(), rand.Next(0,1000),  "<p>Lorem ipsum...</p>",attributesDict.ToDictionary(attr=>attr.Key, attr=>attr.Value)) {
				},
				new NNPromotion(rand.Next(),rand.Next(0,1000),  "<p>Lorem ipsum...</p>",attributesDict.ToDictionary(attr=>attr.Key, attr=>attr.Value)) {
				}
			};
		}

		private static Dictionary<NNAttributeEnum, INNAttribute> CreatePromoAttributes()
		{
			return new Dictionary<NNAttributeEnum, INNAttribute>()
			{
				{ NNAttributeEnum.DateRange, new DateRangeAttribute(DateTime.Now, DateTime.Now)
				{
					AttributeDistance = 0.3,
					AttributeName = "Date Range",
					AttributeType = NNAttributeEnum.DateRange
				}
				},
			   { NNAttributeEnum.ProductScope, new ProductScopeAttribute()
				{
					AttributeDistance = 0.2,
					AttributeName = "Products",
					AttributeType = NNAttributeEnum.ProductScope
				}
				},
			   { NNAttributeEnum.Mechanic, new MechanicAttribute(6)
				{
					AttributeDistance = 0.7,
					AttributeName = "Mechanic",
					AttributeType = NNAttributeEnum.Mechanic
				}
				},
			   { NNAttributeEnum.DiscountDepth, new DiscountDepthAttribute(33)
				{
					AttributeDistance = 0.5,
					AttributeName = "Discount",
					AttributeType = NNAttributeEnum.DiscountDepth
				}
				}
			};
		}

		public static NNConfig GetNearestNeighboursConfig()
		{
			return new NNConfig()
			{
				TopNPromotions = 4,
				NNAttributeWeightsCollection = new Dictionary<NNAttributeEnum, double>()
				{
					{NNAttributeEnum.DiscountDepth, 15 },
					{ NNAttributeEnum.DateRange, 15 },
					{NNAttributeEnum.Mechanic, 40},
					{NNAttributeEnum.ProductScope ,20}
			}
			};
		}
	}
}
