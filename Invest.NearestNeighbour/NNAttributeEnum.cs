﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invest.NearestNeighbour
{
    /// <summary>
    /// The list of all the available attributes
    /// </summary>
    public enum NNAttributeEnum
    {
        DateRange = 10,
        ProductScope = 20,
        CustomerScope = 30,
        DiscountDepth = 40,
        Mechanic = 50,
        Display = 60,
    }
}
