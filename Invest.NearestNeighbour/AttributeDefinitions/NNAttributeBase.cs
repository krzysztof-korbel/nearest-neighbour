﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invest.NearestNeighbour
{
    /// <summary>
    /// Represents an attribute on a promotion that will be used to determing neighbourliness
    /// </summary>
    public abstract class NNAttributeBase: INNAttribute
    {               
		public NNAttributeBase(string name, NNAttributeEnum type)
		{
			AttributeName = name;
			AttributeType = type;
			AttributeDistance = 0;
		}

		// Provided by API
		public string AttributeName { get; set; }      
        public NNAttributeEnum AttributeType { get; set; }        
        public double AttributeDistance { get; set; }

        // Calculated using Nearest Neighbour algorithm       
        public VariableDistance VariableDistance { get; set; }       
    }
}
