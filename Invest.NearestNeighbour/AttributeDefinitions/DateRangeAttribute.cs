﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invest.NearestNeighbour
{
    public class DateRangeAttribute : NNAttributeBase
    {
		public DateRangeAttribute(DateTime startDate, DateTime endDate) : base("Date Range", NNAttributeEnum.DateRange)
		{
			StartDate = startDate;
			EndDate = endDate;
		}

        public DateRangeAttribute(INNAttribute attribute):this(DateTime.Now, DateTime.Now)
        {
            AttributeName = attribute.AttributeName;
            AttributeType = attribute.AttributeType;            
        }

        private DateTime StartDate { get; set; }
        private DateTime EndDate { get; set; }

        public override string ToString()
        {
            return "From: " + StartDate.ToShortDateString() + " To: " + EndDate.ToShortDateString();
        }
    }
}
