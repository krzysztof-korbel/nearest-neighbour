﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invest.NearestNeighbour
{
    /// <summary>
    /// Represents an attribute on a promotion that will be used to determing neighbourliness
    /// </summary>
    public interface INNAttribute
    {
        string AttributeName { get; set; }      
        NNAttributeEnum AttributeType { get; set; }
    }
}
