﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invest.NearestNeighbour
{
    public class MechanicAttribute : NNAttributeBase
    {
		public MechanicAttribute(int value) : base("Mechanic", NNAttributeEnum.Mechanic)
		{
			MechanicValue = value;
		}

		public MechanicAttribute(INNAttribute attribute) :this(0)
        {
            AttributeName = attribute.AttributeName;
            AttributeType = attribute.AttributeType;            
        }

        private int MechanicValue { get; set; }

        public override string ToString()
        {
            return "%" + MechanicValue;
        }
    }
}
