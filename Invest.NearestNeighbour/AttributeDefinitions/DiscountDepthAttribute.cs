﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invest.NearestNeighbour
{
    public class DiscountDepthAttribute : NNAttributeBase
    {
		public DiscountDepthAttribute(int discount) : base("Discount Depth", NNAttributeEnum.DiscountDepth)
		{
			DiscountValue = discount;
		}

		public DiscountDepthAttribute(INNAttribute attribute):this(0)
        {
            AttributeName = attribute.AttributeName;
            AttributeType = attribute.AttributeType;            
        }

        private int DiscountValue { get; set; }

        public override string ToString()
        {
            return DiscountValue + "%";
        }
    }
}
