﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invest.NearestNeighbour
{
    public class ProductScopeAttribute : NNAttributeBase
    {
		public ProductScopeAttribute() : base("Product Scope", NNAttributeEnum.ProductScope) {
			ProductScopeValue = "Coka kola 2l";
		}

		public ProductScopeAttribute(INNAttribute attribute):this()
        {
            AttributeName = attribute.AttributeName;
            AttributeType = attribute.AttributeType;            
        }

        private string ProductScopeValue { get; set; }

        public override string ToString()
        {
            return ProductScopeValue;
        }
    }
}
