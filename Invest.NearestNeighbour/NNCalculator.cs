﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Invest.NearestNeighbour.PromotionAPI;
using System.Collections.Generic;
using System.Diagnostics;

namespace Invest.NearestNeighbour
{
    public class NNCalculator
    {
        public static IEnumerable<NNPromotion> Calculate(int promoBeingTestedId, IEnumerable<INNPromotion> allPromotions, NNConfig config)
        {
			Debug.Assert(config!=null);
			if (config== null)
			{
				return new List<NNPromotion>();
			}
            INNPromotion promoBeingTested = allPromotions.Where(promo => promo.Id == promoBeingTestedId).Single();
			if (promoBeingTested== null)
			{
				Debug.Assert(false);
				return new List<NNPromotion>();
			}
            ICollection<NNPromotion> evaluatedPromotions = new List<NNPromotion>();

            ///Step 1 -> Cycle through each promo in allPromotions to calculate the PRomotionDistance of each promo ->CalculatePromotionDistance
            foreach (var promotion in allPromotions.Where(promo=>promo.Id != promoBeingTestedId))
            {                
                NNPromotion evaluatedPromo = new NNPromotion(promotion);
                evaluatedPromo.PromotionDistance = CalculatePromotionDistance(promotion, promoBeingTested, config.NNAttributeWeightsCollection,config);
                evaluatedPromotions.Add(evaluatedPromo);               
            }

			/// Step 2 -> Find the top X closest promotions (lowest promotion distances)
            var neighbourPromotions= evaluatedPromotions.OrderBy(promo => promo.MatchPercentage).Take(config.TopNPromotions);
			return neighbourPromotions;
        }


		public static int CalculateUpliftPrediction(IEnumerable<NNPromotion> nearestPromotions, NNConfig config)
		{
			var matchWeightSum = nearestPromotions.Select(promo => promo.MatchPercentage).Sum();
			return (int)(nearestPromotions.Select(promo => promo.Uplift * promo.MatchPercentage).Sum() / matchWeightSum);
        }


		/// <summary>
		/// Calculates the PromotionDistance between the two promotions using the Config 
		/// and the various IVariableDistanceCalculators applying the weighting to each variable distance
		/// </summary>
		/// <param name="promotion"></param>
		/// <param name="promoBeingTested"></param>
		/// <param name="nNAttributeWeightsCollection"></param>
		/// <returns></returns>
		private static PromotionDistance CalculatePromotionDistance(INNPromotion promotion, INNPromotion promoBeingTested, Dictionary<NNAttributeEnum, double> nNAttributeWeightsCollection, NNConfig config)
        {
            double distance = 1; // completely different

            // Calculate distance for each attribute
            foreach (var attribute in promotion.NNAttributeCollection)
            {
				var attributeObj = attribute.Value as NNAttributeBase;
				if (attributeObj== null)
				{
					continue;
				}
				attributeObj.VariableDistance = CalculateAttributeDistance(attribute.Value as dynamic, promoBeingTested.NNAttributeCollection[attribute.Key] as dynamic, config);
				distance -= (config.NNAttributeWeightsCollection[attribute.Key]/100) * attributeObj.VariableDistance.Value;
            }

            // Promotion 
            // TODO: how to calculate promotion distance having attribute distances?

            return new PromotionDistance() { Value = distance };
        }

		#region Attribute distance calculation
		/*
		For now, all atributes' distances are calculated the same way
		*/

		/// <summary>
		/// 
		/// </summary>
		/// <param name="attribute"></param>
		/// <param name="attributeBeingTested"></param>
		/// <param name="config"></param>
		/// <returns></returns>
		public static VariableDistance CalculateAttributeDistance(DateRangeAttribute attribute, DateRangeAttribute attributeBeingTested, NNConfig config)
        {
            double distance = 1; // completely different
								 // TODO: implement
			distance = Math.Abs(attribute.AttributeDistance - attributeBeingTested.AttributeDistance);
			return new VariableDistance() { Value = distance };
        }

		public static VariableDistance CalculateAttributeDistance(DiscountDepthAttribute attribute, DiscountDepthAttribute attributeBeingTested, NNConfig config)
		{
			double distance = 1; // completely different
								 // TODO: implement
			distance = Math.Abs(attribute.AttributeDistance - attributeBeingTested.AttributeDistance);
			return new VariableDistance() { Value = distance };
		}

		public static VariableDistance CalculateAttributeDistance(MechanicAttribute attribute, MechanicAttribute attributeBeingTested, NNConfig config)
		{
			double distance = 1; // completely different
								 // TODO: implement
			distance = Math.Abs(attribute.AttributeDistance - attributeBeingTested.AttributeDistance);
			return new VariableDistance() { Value = distance };
		}

		public static VariableDistance CalculateAttributeDistance(ProductScopeAttribute attribute, ProductScopeAttribute attributeBeingTested, NNConfig config)
		{
			double distance = 1; // completely different
								 // TODO: implement
			distance = Math.Abs(attribute.AttributeDistance - attributeBeingTested.AttributeDistance);
			return new VariableDistance() { Value = distance };
		}
		#endregion
	}
}
