﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Invest.NearestNeighbour
{
    public class NNPromotion: INNPromotion
    {
		public NNPromotion() { }

        public NNPromotion(int idParam, double upliftParam, string overview, Dictionary<NNAttributeEnum, INNAttribute> attributes)
		{
			Id = idParam;
			Uplift = upliftParam;
			PromoOverview = overview;
			NNAttributeCollection = attributes;
		}

        public NNPromotion(INNPromotion promo)
        {
            Id = promo.Id;
            Uplift = promo.Uplift;
            PromoOverview = promo.PromoOverview;
            NNAttributeCollection = promo.NNAttributeCollection;
        }

        // Provided by API
        public int Id { get; private set; }
		public double Uplift { get; private set; }
		public string PromoOverview { get; private set; } // will include HTML markup E.g. <STRONG> etc.
        public Dictionary<NNAttributeEnum, INNAttribute> NNAttributeCollection { get; private set; }

        // Calculated using Nearest Neighbour algorithm       
        public PromotionDistance PromotionDistance { get; set; }

		// Using the weightings in the nearest neighbour configuration you can calculate a weighted distance, this % is 1-PromotionDistance (assuming the weighted calc ends up between 0 [identical] and 1[totally different])
		public int MatchPercentage
		{
			get
			{
				return Convert.ToInt32((1 - PromotionDistance.Value) * 100);
			}
		}
	}
}
