﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invest.NearestNeighbour
{
    /// <summary>
    /// Represents the "distance" between two promotions for a given nearestneighbour config
    /// </summary>
    public class PromotionDistance
    {
        /// <summary>
        /// Represents the numeric value of the distance
        /// </summary>
        public double Value { get; set; }
        // Would need to override the standard operators +/- etc. if required
    }
}
