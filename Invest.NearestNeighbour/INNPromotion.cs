﻿using System.Collections.Generic;

namespace Invest.NearestNeighbour
{
    public interface INNPromotion
    {
        int Id { get; }
		double Uplift { get; }
		string PromoOverview { get; } // will include HTML markup E.g. <STRONG> etc.
		Dictionary<NNAttributeEnum, INNAttribute> NNAttributeCollection { get; }
    }
}
