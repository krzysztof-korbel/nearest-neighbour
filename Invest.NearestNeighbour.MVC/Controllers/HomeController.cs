﻿using Invest.NearestNeighbour.MVC.BusinessLogic;
using Invest.NearestNeighbour.MVC.Helpers;
using Invest.NearestNeighbour.MVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Invest.NearestNeighbour.MVC.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			var model = NearestNeighbourCalculator.CalculateNearestNeighbour(PromotionAPI.PromotionAPI.KnownPromoId);
			return View(model);
		}
	
    }
}