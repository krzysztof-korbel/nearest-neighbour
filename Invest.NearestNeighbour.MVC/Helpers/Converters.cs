﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Invest.NearestNeighbour.MVC.Helpers
{
	public class Converters
	{
		/// <summary>
		/// Converts matching percentage to css class string
		/// </summary>
		/// <param name="matchValue"></param>
		/// <returns></returns>
		public static string MatchingToClass(double matchValue)
		{
			if (matchValue <= 0.3333) {
				return "green";
			} else if (matchValue <= 0.6666){
				return "yellow";
			}
			return "red";
		}
	}
}