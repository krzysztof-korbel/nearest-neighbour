﻿using Invest.NearestNeighbour.MVC.Helpers;
using Invest.NearestNeighbour.MVC.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Invest.NearestNeighbour.MVC.BusinessLogic
{
	public class NearestNeighbourCalculator
	{
		public static NearestNeighbourModel CalculateNearestNeighbour(int promotionId)
		{
			var nearestPromotions = NNCalculator.Calculate(promotionId, PromotionAPI.PromotionAPI.GetAllPromotions(PromotionAPI.PromotionAPI.KnownPromoId), PromotionAPI.PromotionAPI.GetNearestNeighboursConfig());
			return ConvertPromotions(nearestPromotions);
		}

		private static NearestNeighbourModel ConvertPromotions(IEnumerable<NNPromotion> promotions) {
			return new NearestNeighbourModel()
			{
				UpliftPrediction = NNCalculator.CalculateUpliftPrediction(promotions, PromotionAPI.PromotionAPI.GetNearestNeighboursConfig()),
				Neighbours = promotions.Select(promo => {
					return new PromotionNeighbour()
					{
						Id = promo.Id,
						Uplift = promo.Uplift,
						MatchPercentage = promo.MatchPercentage,
						PromoOverview = promo.PromoOverview,
						AttributeCollection = promo.NNAttributeCollection.ToDictionary(attr =>
							attr.Value.AttributeName,
							attr => new AttributeDetails()
							{
								Value = attr.Value.ToString(),
								Distance = Converters.MatchingToClass((attr.Value as NNAttributeBase).VariableDistance.Value)
							})
					};
				})
			};
		}
	}
}