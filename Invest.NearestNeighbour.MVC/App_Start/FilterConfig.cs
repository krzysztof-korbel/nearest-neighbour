﻿using System.Web;
using System.Web.Mvc;

namespace Invest.NearestNeighbour.MVC
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
		}
	}
}
