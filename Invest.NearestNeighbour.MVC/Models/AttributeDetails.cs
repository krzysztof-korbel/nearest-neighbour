﻿namespace Invest.NearestNeighbour.MVC.ViewModels
{
	public class AttributeDetails
	{
		public string Value { get; set; }
		public string Distance { get; set; }

	}
}