﻿using System.Collections.Generic;

namespace Invest.NearestNeighbour.MVC.ViewModels
{
	public class PromotionNeighbour
	{
		public int Id { get; set; }

		public double Uplift { get; set; }

		public int MatchPercentage {get;set;}

		public string PromoOverview { get; set; }
		public Dictionary<string, AttributeDetails> AttributeCollection { get; set; }
		
	}
}