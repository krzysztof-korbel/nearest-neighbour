﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Invest.NearestNeighbour.MVC.ViewModels
{
	/// <summary>
	/// Structure that holds data about promotion and its nearest neighbours
	/// </summary>
	public class NearestNeighbourModel
	{
		public int UpliftPrediction { get; set; }

		public IEnumerable<PromotionNeighbour> Neighbours { get; set; }
	}

	
}